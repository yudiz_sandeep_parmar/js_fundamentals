let x = 1;

while (x < 10) {
	console.log(`X is increasing ${x}`);
	x++;
}

let stock = 10;

do {
	console.log(`Market ${stock}`);
	stock--;
} while (stock > 0);

for (let index = 5; index > 0; index--) {
	console.log('Index is ' + index);
}
let interns = ['x', 'y', 'z', 'a', 'b', 'c'];
let leads = [1, 2, 34, 5];
let newArray = [];
interns.push('Bhargav');
interns.push('Sandeep');
interns.push('Joey');
interns.pop();
console.log(interns);
const people = { name: 'David' };
let people2 = people;
people2.name = 'Guetta';
console.log(people.name);
console.log(people2.name);
let n = 10 + null;
console.log(n);
let n2 = 20 + undefined;
console.log(n2);

const people = [
	{
		name: 'abc',
		age: 20,
		position: 'developer',
		salary: 500,
	},
	{
		name: 'abcd',
		age: 21,
		position: 'developer',
		salary: 400,
	},
	{ name: 'abcde', age: 22, position: 'developer', salary: 800 },
];
people.forEach((people) => {
	console.log(people.age);
});

const data = people.map((x) => {
	console.log(x.name);
	console.log(x.age);
	console.log(x.position);
});

const total = people.reduce((acc, currItem) => {
	console.log(`total ${acc}`);
	console.log(`current money: ${currItem.salary}`);
	acc += currItem.salary;
	return acc;
}, 500);
