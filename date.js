const days = [
	"Sunday",
	"Monday",
	"Tuesday",
	"Wednesday",
	"Thursday",
	"Friday",
	"Saturday",
];

const date = new Date();
const month = date.getMonth();
console.log(date.getDay());
const day = date.getDay();

const string = `${
	days[day]
}, ${date.getDate()}, ${month} ${date.getFullYear()}`;

console.log(string);
document.body.innerHTML = string;
