// const h1 = document.getElementById('title');
// h1.style.color = 'red';

// const btn = document.getElementById('btn');

// btn.style.color = 'pink';
// btn.style.backgroundColor = 'rgb(31, 134, 202)';

// const headings = document.getElementsByTagName('h1');
// headings[0].style.color = 'blue';

// const btnClass = document.getElementsByClassName('btn');
// btnClass[0].style.color = 'pink';

// const btn = document.querySelector('#btn');
// btn.style.backgroundColor = 'rgb(31, 134, 202)';
// btn.style.color = 'pink';

// const first = document.querySelector('.first');
// const idValue = first.getAttribute('id');
// console.log(idValue);

// const link = document.getElementById('link');
// const showLink = link.getAttribute('href');
// console.log(showLink);

// const last = link.nextElementSibling;
// last.setAttribute('class', 'first');
// last.textContent = 'i also have a class of first';

// const links = document.querySelectorAll('.first');
// console.log(links);

// const first = document.getElementById('first');
// const second = document.getElementById('second');
// const third = document.getElementById('third');

// const classValue = first.className;
// console.log(classValue);

// second.className = 'colors text';
// third.classList.add('colors', 'text');

// const result = document.querySelector('#result');
// const first = document.querySelector('.red');

// const bodyDiv = document.createElement('div');
// const text = document.createTextNode('body div from js');
// bodyDiv.appendChild(text);
// document.body.appendChild(bodyDiv);
// document.body.insertBefore(bodyDiv, result);
// result element
// const heading = document.createElement('h3');
// const headingText = document.createTextNode('dynamic h3');
// heading.appendChild(headingText);
// heading.classList.add('blue');
// result.insertBefore(heading, first);
// document.body.appendChild(heading);

// const smallHeading = document.createElement('h5');
// const smallText = document.createTextNode('replaced h5');
// smallHeading.classList.add('red');
// smallHeading.appendChild(smallText);
// document.body.replaceChild(smallHeading, bodyDiv);

// console.log(result.children);

// const heading = document.createElement('h2');
// heading.innerText = `This is dynamic H2`;
// document.body.prepend(heading);

// const result = document.querySelector('#result');
// result.remove();

// const heading = document.querySelector('h1');
// result.removeChild(heading);
// console.log(heading);

// const list = document.getElementById('first');
// const div = document.getElementById('second');
// const item = document.querySelector('.item');

// const randomValue = 'random';
// console.log(div.textContent);
// console.log(list.innerHTML);
// const ul = document.createElement('ul');
// ul.innerHTML = `<li class="item">ul ${randomValue}</li>
// 			<li>ul list item 2</li>`;
// document.body.appendChild(ul);

const random = document.querySelector('.random');
// console.log(random.style);
// random.style.backgroundColor = 'blue';
// random.style.color = 'white';
// random.style.fontSize = '3rem';
// random.style.textTransform = 'capitalized';

random.classList.add('title');
