// Event Listeners
// const btn = document.querySelector('.btn');
// const heading = document.querySelector('h2');

// const changeColors = () => {
// 	let hasClass = heading.classList.contains('red');
// 	if (hasClass) {
// 		heading.classList.remove('red');
// 	} else {
// 		heading.classList.add('red');
// 	}
// };

// btn.addEventListener('click', changeColors);

// const heading = document.querySelector('h1');
// const btn = document.querySelector('.btn');

// btn.addEventListener('click', () => {
// 	console.log('clicked');
// });

// btn.addEventListener('mouseup', () => {
// 	console.log('up');
// });

// btn.addEventListener('mousedown', () => {
// 	console.log('down');
// });

// heading.addEventListener('mouseenter', () => {
// 	heading.classList.add('blue');
// });

// heading.addEventListener('mouseleave', () => {
// 	heading.classList.add('red');
// });

// const nameInput = document.getElementById('name');

// nameInput.addEventListener('keypress', () => {
// 	console.log('key pressed');
// });

// nameInput.addEventListener('keydown', () => {
// 	console.log('key down');
// });

// nameInput.addEventListener('keyup', () => {
// 	console.log('key up');
// });

// const heading = document.querySelector('h1');
// const btn = document.querySelector('.btn');
// const link = document.getElementById('link');

// heading.addEventListener('click', (event) => {
// 	event.currentTarget.classList.add('blue');
// 	console.log(this);
// });

// btn.addEventListener('click', (event) => {
// 	event.currentTarget.classList.add('blue');
// });

// link.addEventListener('click', (event) => {
// 	event.preventDefault();
// });

// const btn = document.querySelectorAll('.btn');

// btn.forEach((b) => {
// 	b.addEventListener('click', (e) => {
// 		console.log(e.currentTarget);
// 		// e.currentTarget.style.color = 'black';
// 		e.target.style.color = 'black';
// 	});
// });

// Propagation and bubbling

// const container = document.querySelector('.container');
// const list = document.querySelector('.list-items');

// const showBubbling = (e) => {
// 	console.log('current target', e.currentTarget);
// 	console.log('target', e.target);
// 	if (e.target.classList.contains('link')) {
// 		console.log('clicked on link');
// 	}
// };

// function stopPropagation(e) {
// 	console.log('clicked on list');
// 	e.stopPropagation();
// }

// container.addEventListener('click', showBubbling);
// list.addEventListener('click', showBubbling, { capture: true });

// forms

// const form = document.getElementById('form');
// const fname = document.getElementById('text');
// const password = document.getElementById('password');

// form.addEventListener('submit', (e) => {
// 	e.preventDefault();
// 	console.log('submitted');
// 	console.log(fname.value);
// 	console.log(password.value);
// });
